# SnowFlake

## 说明

分布式ID生成类

```php
<?php

require_once "./vendor/autoload.php";
use QianLong\SnowFlake\SnowFlake;

class Index
{
    public function test():
    {
        echo (new SnowFlake)->createId();
    }
}

```
